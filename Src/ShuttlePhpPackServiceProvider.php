<?php

namespace Shuttle\PhpPack\Traits;

use Illuminate\Support\ServiceProvider;

class ShuttlePhpPackServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->make('Shuttle\PhpPack\Traits');
    }

    /**
     *
     */
    public function boot()
    {
    }
}
