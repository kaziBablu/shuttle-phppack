<?php

namespace Shuttle\PhpPack\Traits;

use Carbon\Carbon;

trait ShuttlePhpPackTrait
{
    /**
     * @return string|string[]
     */
    public function createEncodeHeader()
    {
        $header = json_encode(['typ' => 'JWT', 'alg' => 'HS256']);
        $base64UrlHeader = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($header));
        return $base64UrlHeader;
    }

    /**
     * @param Request $request
     * @return string|string[]
     */
    public function createEncodePayload($payload)
    {
        $payload = json_encode($payload);
        $base64UrlPayload = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($payload));
        return $base64UrlPayload;
    }

    /**
     * @return string
     */
    public function createSignatureHas($payload)
    {
        $signature = hash_hmac('sha256', $this->createEncodeHeader() . "." . $this->createEncodePayload($payload), getenv('JWT_SECRET'), true);

        return $signature;
    }

    /**
     * @return string|string[]
     */
    public function encodeSignatureBase64Url($payload)
    {
        $base64UrlSignature = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($this->createSignatureHas($payload)));

        return $base64UrlSignature;
    }

    /**
     * @return mixed
     */
    public function createJwtToken($payload)
    {
        $jwt = $this->createEncodeHeader() . "." . $this->createEncodePayload($payload) . "." . $this->encodeSignatureBase64Url($payload);
        return $jwt;

    }

    public function splitTokenAndVerify($token)
    {
        $tokenParts = explode('.', $token);
        $header = base64_decode($tokenParts[0]);
        $payload = base64_decode($tokenParts[1]);
        $signatureProvided = $tokenParts[2];

        $expiration = Carbon::createFromTimestamp(json_decode($payload)->expires);
        $tokenExpired = (Carbon::now()->diffInSeconds($expiration, false) < 0);

        $base64UrlHeader = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($header));
        $base64UrlPayload = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($payload));
        $signature = hash_hmac('sha256', $base64UrlHeader . "." . $base64UrlPayload, getenv('JWT_SECRET'), true);
        $base64UrlSignature = str_replace(['+', '/', '='], ['-', '_', ''], base64_encode($signature));

        $signatureValid = ($base64UrlSignature === $signatureProvided);

        if(($tokenExpired) && (!$signatureValid)){
            return false;
        }else{
            return true;
        }

//        if ($tokenExpired) {
//            echo "Token has expired.\n";
//        } else {
//            echo "Token has not expired yet.\n";
//        }
//
//        if ($signatureValid) {
//            echo "The signature is valid.\n";
//        } else {
//            echo "The signature is NOT valid\n";
//        }
    }
}
